﻿using System.Collections.Generic;
using Tarantool.Client.Model;

namespace MemoryCacher
{
    public interface IMemCacher<T>
    {
        List<Tuple<uint, uint, string, string>> SelectPrimary(uint index);
        T SelectSecondary(string index);
        void Insert(uint primaryKey, uint count, string data, string secondaryKey);
        void Delete(uint a);
    }
}
