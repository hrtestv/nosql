﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace MemoryCacher
{
    public class Serialize<T> where T : class
    {
        public string Serialise(T item)
        {
            if (item == null)
            {
                throw new ArgumentNullException(nameof(item));
            }
                string json = JsonConvert.SerializeObject(item);
                return json;
        }

        public T Deserialise(string json)
        {
            if (json != null)
            {
                var employee = JsonConvert.DeserializeObject<T>(@json);
                return employee;
            }
            return null;
        }
    }
}
