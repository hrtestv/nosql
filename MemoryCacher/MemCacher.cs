﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using Tarantool.Client;
using Tarantool.Client.Model;
using Tarantool.Client.Model.Enums;

namespace MemoryCacher
{
    public class MemCacher<T> : IMemCacher<T> where T : class
    {
        private readonly Index _indexPrimary;
        private readonly Index _indexSecondary;
        private readonly Serialize<T> _serialize;

        public MemCacher(string ip, int port, string nameSpace)
        {
            _serialize = new Serialize<T>();

            var options = new ConnectionOptions()
            {
                EndPoint = new IPEndPoint(IPAddress.Parse(ip), port)
            };
            var tarantoolClient = new Box(options);

            tarantoolClient.Connect().GetAwaiter().GetResult();

            var schema = tarantoolClient.GetSchema();

            

            //var t = schema.CreateSpaceAsync(nameSpace).Result;
            //t.CreateIndex();
            //Index indf = new Index(1u, t.Id, nameSpace, false, IndexType.Hash, null);

            //Space p = new Space()


            var space = schema.GetSpace(nameSpace).GetAwaiter().GetResult();

            var index = space.GetIndex("primary").GetAwaiter().GetResult();
           // var index2 = space.GetIndex("secondary").GetAwaiter().GetResult();

            _indexPrimary = index;
            //_indexSecondary = index2;
        }

        public List<Tuple<uint, uint, string, string>> SelectPrimary(uint index)
        {
            var existing = _indexPrimary.Select<Tuple<uint>, Tuple<uint, uint, string, string>>(Tuple.Create(index), new SelectOptions { Iterator = Iterator.Ge }).Result;
            var y = existing.Data.ToList();
            return y;
        }

        public T SelectSecondary(string index)
        {
            var existing = _indexSecondary.Select<Tuple<string>, Tuple<uint, string, string>>(Tuple.Create(index)).Result;
            var y = existing.Data.ToList();
            return _serialize.Deserialise(y[0].Item3);
        }

        public async void Insert(uint primaryKey, uint count, string data, string secondaryKey)
        {
            //var json = _serialize.Serialise(data);
            await _indexPrimary.Insert(Tuple.Create(primaryKey, count, data, secondaryKey));
        }

        public async void Delete(uint index)
        {
            var existing = _indexPrimary.Select<Tuple<uint>, Tuple<uint, uint, string, string>>(Tuple.Create(index)).Result;
            if (existing.Data.Any())
            {
                await _indexPrimary.Delete<Tuple<uint>, Tuple<uint, uint, string, string>>(Tuple.Create(index));
            }
        }
    }
}
