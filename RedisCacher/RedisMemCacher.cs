﻿using System.Collections.Generic;
using System.Linq;
using StackExchange.Redis;
//ServiceStack платная библиотека

namespace RedisCacher
{
    public class RedisMemCacher
    {
        private readonly ConnectionMultiplexer _connection;
        private IDatabase _db;
        
        public RedisMemCacher()
        {
            var redisConfig = ConfigurationOptions.Parse("localhost,abortConnect=false");
            redisConfig.SyncTimeout = 1000000;
            _connection = ConnectionMultiplexer.Connect(redisConfig);
            _db = _connection.GetDatabase();
        }

        public void InsertItem(long key, long population, string company)
        {
            _db.StringSet($"key2:{key}", $"Population: {population}, Company:{company}, First:Hello World");
        }

        public RedisValue[] SelectItems(IEnumerable<string> keys)
        {
            var redisKeys = keys.Select(x => (RedisKey)x).ToArray();
            var result = _db.StringGet(redisKeys);
            //var dict = new Dictionary<string, string>(StringComparer.Ordinal);
            //for (var index = 0; index < redisKeys.Length; index++)
            //{
            //    var value = result[index];
            //    dict.Add(redisKeys[index], value == RedisValue.Null ? default(string) : (string)value);
            //}
            //return dict;
            return result;
        }
    }
}
