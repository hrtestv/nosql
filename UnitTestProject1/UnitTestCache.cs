﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MemoryCacher;
using RedisCacher;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTestCache
    {
        [TestMethod]
        public void SelectTest()
        {
            var example = new Example();
            example.Int = 25;
            example.Str = "Hello World";
            IMemCacher<Example> a = new MemCacher<Example>("192.168.148.129", 3303, "tester6");

            a.SelectPrimary(20000);
            a.SelectSecondary("Hello World1");
        }

        [TestMethod]
        public void InsertTestTarantool() // 00:07:46.59
        {
            Stopwatch stopWatch = new Stopwatch();

            var example = new Example();
            example.Int = 25;
            example.Str = "Hello World";
            IMemCacher<Example> a = new MemCacher<Example>("192.168.148.129", 3303, "tester11");
            stopWatch.Start();
            for (uint i = 0; i < 1000000; i++)
            {
                a.Insert(i, i + 100, "Hello World", $"Script number #{i}");
            }
            stopWatch.Stop();
            TimeSpan ts = stopWatch.Elapsed;
            string elapsedTime = $"{ts.Hours:00}:{ts.Minutes:00}:{ts.Seconds:00}.{ts.Milliseconds / 10:00}";
        }

        [TestMethod]
        public void InsertTestTarantoolThrow() // 00:06:23.78 // на 100 потоках после 40 минут работы комп потух
        {

            Stopwatch stopWatch = new Stopwatch();

            stopWatch.Start();

            int count = 1;
            Thread[] handles = new Thread[10];
            while (count < 11)
            {
                handles[count - 1] = new Thread(ThreadFunction);
                handles[count - 1].Start(count * 100000);
                ++count;
            }

            foreach (var pair in handles)
            {
                pair.Join();
            }

            stopWatch.Stop();
            TimeSpan ts = stopWatch.Elapsed;
            string elapsedTime = $"{ts.Hours:00}:{ts.Minutes:00}:{ts.Seconds:00}.{ts.Milliseconds / 10:00}";
        }

        private void ThreadFunction(object obj)
        {
            IMemCacher<Example> a = new MemCacher<Example>("192.168.148.129", 3303, "tester");
            for (uint i = Convert.ToUInt32(obj) - 100000; i < Convert.ToUInt32(obj); i++)
            {
                a.Insert(i, i + 100, "Hello World", $"Script number #{i}");
            }
        }

        [TestMethod]
        public void SelectRangeTest()//990000 записей за 00:00:10.19
        {
            IMemCacher<Example> a = new MemCacher<Example>("192.168.148.129", 3303, "tester");
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            var t = a.SelectPrimary(10000);
            stopWatch.Stop();
            TimeSpan ts = stopWatch.Elapsed;
            string elapsedTime = $"{ts.Hours:00}:{ts.Minutes:00}:{ts.Seconds:00}.{ts.Milliseconds / 10:00}";
        }

        [TestMethod]
        public void DeleteTest()
        {
            IMemCacher<Example> a = new MemCacher<Example>("192.168.148.129", 3303, "tester10");
            for (uint i = 0; i < 1000000; i++)
            {
                a.Delete(i);
            }
        }

        [TestMethod]
        public void RedeisInsertTest()//00:06:00.62
        {
            RedisMemCacher memCacher = new RedisMemCacher();
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            for (uint i = 0; i < 1000000; i++)
            {
                memCacher.InsertItem(i, i * 100, "Microsoft");
            }
            stopWatch.Stop();
            TimeSpan ts = stopWatch.Elapsed;
            string elapsedTime = $"{ts.Hours:00}:{ts.Minutes:00}:{ts.Seconds:00}.{ts.Milliseconds / 10:00}";
        }

        [TestMethod]
        public void InsertTestRedisThrow() // 00:04:20.40 на 10 потоках
        {
            Stopwatch stopWatch = new Stopwatch();

            stopWatch.Start();

            int count = 1;
            Thread[] handles = new Thread[10];
            while (count < 11)
            {
                handles[count - 1] = new Thread(ThreadFunctionRedis);
                handles[count - 1].Start(count * 100000);
                ++count;
            }
            foreach (var pair in handles)
            {
                pair.Join();
            }
            stopWatch.Stop();
            TimeSpan ts = stopWatch.Elapsed;
            string elapsedTime = $"{ts.Hours:00}:{ts.Minutes:00}:{ts.Seconds:00}.{ts.Milliseconds / 10:00}";
        }

        private void ThreadFunctionRedis(object obj)
        {
            RedisMemCacher memCacher = new RedisMemCacher();
            for (uint i = Convert.ToUInt32(obj) - 100000; i < Convert.ToUInt32(obj); i++)
            {
                memCacher.InsertItem(i, i * 100, "Apple");
            }
        }

        [TestMethod]
        public void SelectTestRedis() //00:00:27.97
        {
            Stopwatch stopWatch = new Stopwatch();
            
            RedisMemCacher memCacher = new RedisMemCacher();
            List<string> keys = new List<string>();
            for (int i = 1; i < 900001; i++)
            {
                keys.Add($"key2:{i}");
            }
            var enumerator = (IEnumerable<string>)keys;
            stopWatch.Start();
            var result = memCacher.SelectItems(enumerator);
            stopWatch.Stop();
            TimeSpan ts = stopWatch.Elapsed;
            string elapsedTime = $"{ts.Hours:00}:{ts.Minutes:00}:{ts.Seconds:00}.{ts.Milliseconds / 10:00}";
        }
    }

    public class Example
    {
        public string Str { get; set; }
        public int Int { get; set; }
    }
}
